import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import time
import RPi.GPIO as GPIO


class Roletna:
    """
    Klasa koja opisuje rad sistema za kontrolu pozicije roletne na osnovu
    kolicine svetla koju registruje senzor
    """

    def __init__(self):
        GPIO.cleanup()
        self.pwmPin = 18
        GPIO.setmode(GPIO.BCM)  # Setovanje BCM seme numeracije
        GPIO.setup(self.pwmPin, GPIO.OUT)  # Setovanje pwmPin da bude izlaznog tipa
        self.pwm = GPIO.PWM(self.pwmPin, 50)  # Podesavanje ucestanosti na 50Hz
        self.pwm.start(5.0)
        spi_port   = 0
        spi_device = 0
        self.mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(spi_port, spi_device))
        self.state = True  # True znaci da je roletna podignuta

    def __del__(self):
        self.pwm.stop()
        GPIO.cleanup()

    def podigni_roletnu(self):
        """
        Metoda koja podize roletnu
        """
        self.pwm.ChangeDutyCycle(8.0)  # Pomeranje motora na gore
	    time.sleep(0.3)

    def spusti_roletnu(self):
        """
        Metoda koja podize roletnu
        """
        self.pwm.ChangeDutyCycle(5.0)  # Pomeranje motora na gore
        time.sleep(0.3)

    def scan(self):

        while True:
            read = self.mcp.read_adc(7)
            print(read)
            if read < 500 and not self.state:
                print('Dizem roletnu')
                self.podigni_roletnu()
                self.state = True
            elif self.state and read > 500:
                print('Spustam roletnu')
                self.spusti_roletnu()
                self.state = False
            time.sleep(0.3)


if __name__ == "__main__":
    """
    Testiranje funkcionalnosti
    """
    temp = Roletna()
    temp.scan()
    input('Unesi nesto za kraj programa\n')
    GPIO.cleanup()
