import RPi.GPIO as GPIO
from datetime import datetime
import logging


class Temperatura:
    """
    Klasa koja opisuje rad temperaturnog senzora i ventilatora koji hladi prostoriju
    """

    def __init__(self, senzor_pin, ventilator_pin):
        """
        Potrebno je definisati ulazni pin gde je doveden digitalni
        izlaz sa senzora temperature.
        Obratiti pazjnu na numeraciju jer se koristi BCD rezim numeracije
        """
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()
        # Pin na koji je doveden senzor
        self.senzor = senzor_pin

        # Pin na koji se kaci ventilator
        self.ventilator = ventilator_pin

        # Podesavanje pinova
        GPIO.setup(self.senzor, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.ventilator, GPIO.OUT)
        GPIO.output(self.ventilator, GPIO.LOW)

        # Podesavanje interapt-a
        # Vreme nakon kojeg se smatra da je aktivacija stvarna je 400ms
        GPIO.add_event_detect(self.senzor, GPIO.BOTH, callback=self.senzor_temperature,
                              bouncetime=400)
        GPIO.setup(self.ventilator, GPIO.OUT)
        GPIO.output(self.ventilator, GPIO.LOW)

        # Inicijalizacija fajla u koji cuvamo sve sta se desava
        filename = str(datetime.now()).replace(' ', '_') + '_temperatura.log'
        logging.basicConfig(filename=filename, level=logging.DEBUG)
        logging.info('Senzor temperature inicijalizovan uspesno')
        print 'Inicijalizacija senzora zavrsena'

    def senzor_temperature(self, channel):
        """
        Metoda koja se poziva prilikom interapta koji dolazi sa temperaturnog senzora.
        """
        if GPIO.input(self.ventilator):
            time = datetime.now().time()
            poruka = 'Pad temperature detektovan >> ' + str(time)
            GPIO.output(self.ventilator, GPIO.LOW)
            # Upisivanje poruke u fajl u kojem pratimo stanje programa
            logging.info(poruka)

        else:
            vreme = datetime.now().time()
            poruka = 'Povisena temperatura detektovana >> ' + str(vreme)
            GPIO.output(self.ventilator, GPIO.HIGH)
            # Upisivanje poruke u fajl u kojem pratimo stanje programa
            logging.info(poruka)

        return 0


if __name__ == "__main__":
    """
    Testiranje programa
    """
    temp = Temperatura(18, 21)
    raw_input('unesi nesto\n')
    GPIO.cleanup()
