# Zdravo

Ovo je wiki stranica o kursu za pravljenje pametne kuće. Sve bitne informacije o kodu, elektronici 
i osalim komponentama mogu se naći ovde.

## Elektronika

U ovom kursu koristićemo [RaspberryPi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/). 
To je jedan od najpopularnijih IoT uređaja sa velikom zajednicom koja može pomoći prilikom razvoja 
projekta. Osim velike zajednice na raspolaganju je i veliki broj dodatnih senzora, aktuatora, 
indikatora i ostale prateće elektronike


Pored RaspbrerryPi pločice koja ima ulogu glavnog kontrolera tokom ovog kursa koristi se i propratna elektornika. 

1. Senzori
2. Indikatori
3. Izvori napajanja
4. Aktuatori


## Softver

Cela aplikacija je realizovana upotrebom Python programskog jezika. Preuzimanje koda se može izvršiti na sledeći način


```bash
git clone https://gavriloandric@bitbucket.org/gavriloandric/smart_home_course.git
```

# Sadržaj

1. [Klimatizacija prostora](https://bitbucket.org/gavriloandric/smart_home_course/wiki/klimatizacija)
2. [Detektor požara](https://bitbucket.org/gavriloandric/smart_home_course/wiki/Detektor%20pozara)
3. [Roletne](https://bitbucket.org/gavriloandric/smart_home_course/wiki/Roletne)