import RPi.GPIO as GPIO
from datetime import datetime
import logging
import time


class DetektorPozara:
    """
    Klasa koja sadrzi sve funkcionalnosti potrebne za koriscenje garaznog sistema
    """

    def __init__(self, pistalo_pin=14, senzor_pin=15):
        """
        Potreno je definisati sve pinove potrebne za mehanizam
        """
        # Podesavanja ulaznih pinova, BDC rezim numeracije se koristi
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        # Resetovanje svih pinova
        GPIO.cleanup()

        # Pin na koji je dovedeno pistalo
        self.pistalo_pin = pistalo_pin

        # pistalo je output pind
        GPIO.setup(self.pistalo_pin, GPIO.OUT)

        self.p = GPIO.PWM(self.pistalo_pin, 1000)

        # Pin na koji se kaci sensor
        self.senzor_pin = senzor_pin

        # Senzor je input pin
        GPIO.setup(self.senzor_pin, GPIO.IN)

        # Pocetno stanje pina je LOW, jer zelimo da bude ugasen na pocetku
        GPIO.output(self.pistalo_pin, GPIO.LOW)

        # Podesavanje prekida za ocitavanje sa senzora
        GPIO.add_event_detect(self.senzor_pin, GPIO.RISING, callback=self.ocitaj_senzor, bouncetime=400)
        # Inicijalizacija fajla u koji cuvamo sve sta se desava
        filename = str(datetime.now()).replace(' ', '_') + '_garaza.log'
        logging.basicConfig(filename=filename, level=logging.DEBUG)
        logging.info('Garaza je incijalizovana uspesno')
        print('Garaza je inicijalizovana uspesno')
        print('Pistalo pin >> ', self.pistalo_pin)
        print('Senzor pin >> ', self.senzor_pin)

    def upali_pistalo(self):
        """
        Metoda za paljenje pistaloa. Ukoliko je pistalo vec upaljen prijavljuje i loguje gresku.
        """

        print('Palimo pistalo')
        
        GPIO.output(self.pistalo_pin, True) 
        self.p.start(100)
        self.p.ChangeDutyCycle(60)
        print('Pistalo je uspesno upaljeno')
        logging.info('Pistalo je uspesno upaljeno\n')

    def ugasi_pistalo(self):
        """
        Metoda za gasenje pistala. Ukoliko je pistalo vec ugaseno prijavljuje i loguje gresku.
        """
        print('Gasimo pistalo')
        self.p.stop()
        print('pistalo je uspesno ugaseno')
        logging.info('pistalo je uspesno ugaseno')

    def ocitaj_senzor(self, channel):
        """
        Metoda za ocitavanja sa senzora
        """
        # Zelimo da pistalo pisti 1 sekndu
        self.upali_pistalo()
        time.sleep(0.5)
        self.ugasi_pistalo()
        print('VATRAAAAAA')
        logging.info('Skok na senzoru')
 

if __name__ == "__main__":

    garazica = DetektorPozara()
    garazica.upali_pistalo()
    time.sleep(2)
    garazica.ugasi_pistalo()
    input('Unesi bilo sta za kraj\n')
    GPIO.cleanup()
