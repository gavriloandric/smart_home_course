import time
# noinspection PyUnresolvedReferences
import RPi.GPIO as GPIO
from datetime import datetime
import logging


# noinspection PyUnusedLocal
class Svetla:
    def __init__(self, spavaca, hodnik, dugme):
        """
        Potrebno je definisati izlazne pinove za svetla u spavacoj sobi i hodniku
        Obratiti pazjnu na numeraciju jer se koristi BCD rezim numeracije
        """
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()

        # Pin na koji je dovedeno svetlo iz spavace sobe
        self.spavaca = spavaca

        # Pomocna promenljiva za stanje svetla u spavacoj sobi
        self.stanje_spavaca = 0

        # Pin na koji je dovedeno svetlo iz hodnika
        self.hodnik = hodnik

        # Pomocna promenljiva za stanje svetla u hodniku
        self.stanje_hodnik = 0

        # Pin na koji nam je zakaceno dugme
        self.dugme = dugme

        # Podesavanje pinova
        GPIO.setup(self.spavaca, GPIO.OUT)
        GPIO.setup(self.hodnik, GPIO.OUT)
        GPIO.setup(self.dugme, GPIO.IN)

        # Pocetna stanja pinova
        GPIO.output(self.spavaca, GPIO.LOW)
        GPIO.output(self.hodnik, GPIO.LOW)

        # Podesavanje prekida za ocitavanje sa senzora
        GPIO.add_event_detect(self.dugme, GPIO.RISING, callback=self.regulisi_svetlo_u_hodniku, bouncetime=400)

        # Inicijalizacija fajla u koji cuvamo sve sta se desava
        filename = str(datetime.now()).replace(' ', '_') + '_svetla.log'
        logging.basicConfig(filename=filename, level=logging.DEBUG)
        logging.info('Sistema svetala je uspeno inicijalizovan')
        print ('Inicijalizacija je uspesno zavrsena')

    def upali_svetlo_u_hodniku(self):
        """
        Metoda za paljenja svetla u hodniku, vrednost pina je sacuvana u self.hodnik
        """
        GPIO.output(self.spavaca, GPIO.HIGH)
        self.stanje_hodnik = 1

    def ugasi_svetlo_u_hodniku(self):
        """
        Metoda za gasenje svetla u hodniku, vrednost pina je sacuvana u self.hodnik
        """
        GPIO.output(self.spavaca, GPIO.LOW)
        self.stanje_hodnik = 0

    def regulisi_svetlo_u_hodniku(self, channel):
        """
        Metoda koja pali svetlo ako je ugaseno ili gasi svetlo ako je vec upaljeno
        """
        if self.stanje_hodnik == 0:
            self.upali_svetlo_u_hodniku()
        else:
            self.ugasi_svetlo_u_hodniku()

    def upali_svetlo_u_spavacoj(self):
        """
        Metoda za paljenje svetla u spavacoj, vrednost pina je sacuvana u self.spavaca
        """
        GPIO.output(self.spavaca, GPIO.HIGH)
        self.stanje_spavaca = 1

    def ugasi_svetlo_u_spavacoj(self):
        """
        Metoda za gasenje svetla u spavacoj, vrednost pina je sacuvana u self.spavaca
        """
        GPIO.output(self.spavaca, GPIO.LOW)
        self.stanje_spavaca = 0

    def regulisi_svetlo_u_spavacoj(self):
        """
        Metoda koja pali i gasi svetlo u spavacoj sobi
        """
        if self.stanje_spavaca == 0:
            self.upali_svetlo_u_spavacoj()
        else:
            self.ugasi_svetlo_u_spavacoj()

    def vremenska_kontrola_spavace_sobe(self, period):
        """
        Metoda koja periodicno pali i gasi svetlo u spavacoj sobi, argument period odredjuje vremenski period
        """
        while 1:
            self.regulisi_svetlo_u_spavacoj()
            time.sleep(period)


if __name__ == "__main__":
    """
    Testiranje programa
    """
    sistem = Svetla(14, 14, 18)
    input()
